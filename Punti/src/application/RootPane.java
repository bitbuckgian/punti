package application;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import data.Content;
import data.Item;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media;

/*
 * Il pannello radice (root) della scena
 */
public class RootPane extends BorderPane {

	protected static String userDataPath = ".";
	private Pane currentPane;
	private Button homeButton;
	private VideoPane videoPane;

	private Content content;

	public RootPane() {

		super();

		TextInputDialog dialog = new TextInputDialog(".");
		dialog.setTitle("Input path");
		dialog.setHeaderText("Scrivi il path della cartella dei video che vuoi studiare");
		dialog.setContentText("Path della cartella:");

		// Traditional way to get the response value.
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()){
		   userDataPath = result.get();
		}

		//lista nomi dei video presenti nella cartella dell'utente

		File file = new File(userDataPath);

		//il filtro serve per scartare tutti i file che non sono mp4
		File[] videos = file.listFiles(new FileFilter() {

			@Override
			public boolean accept(File pathname) {

				return pathname.getName().endsWith(".mp4");
			}
		});




		//creazione home pane

		ArrayList<String> names = new ArrayList<>();

		for (int i = 0; i < videos.length; i++) {

			names.add(withoutExtension(videos[i].getName()));

		}


		HomePane homePane = new HomePane(names);
		setCenter(homePane); //home pane è al centro del root pane

		ContentPane puntiPane = new ContentPane();

		videoPane = new VideoPane();
		videoPane.setPropertyToBind(widthProperty());

		NuovoPuntoPane nuovoPuntoPane = new NuovoPuntoPane();

///////////////////////////// HANDLERS ////////////////////////////////////////////////////

		//registrazione handler dei bottoni dell'home pane

		homePane.setHandler(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				//rimuove l'home pane
				getChildren().remove(homePane);

				//salva i contenuti correnti
				if(content != null) PersistenceManager.save(content);

				//carica i nuovi contenuti
				String videoName = ((Button)event.getSource()).getText();

				content = (PersistenceManager.loadContent(videoName));
				
				puntiPane.setContent(content);

				//carica il video
				String source = new File(userDataPath + "/" + videoName + ".mp4").toURI().toString();
				Media media = new Media(source);

				//passo il media al video pane
				videoPane.setMedia(media);

				//cambio il valore della property visible
				videoPane.setVisible(true);

				//metto il video pane nel root pane e visualizzo anche l'home button
				setCenter(videoPane);
				setTop(homeButton);

				if(currentPane != null) currentPane.setVisible(false);

				//memorizzo il pannello inserito per poterlo successivamente rimuovere
				currentPane = videoPane;

			}
		});

		//handlers del video pane

		videoPane.setNuovoPuntoHandler(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				videoPane.setVisible(false);
				currentPane = nuovoPuntoPane;
				String time = String.valueOf(videoPane.getCurrentTime());
				nuovoPuntoPane.setTime(time);
				nuovoPuntoPane.setVisible(true);
				setCenter(nuovoPuntoPane);

			}
		});



		videoPane.setPuntiHandler(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				videoPane.setVisible(false);
				currentPane = puntiPane;
				currentPane.setVisible(true);
				setCenter(puntiPane);

			}
		});

		//handler del punti pane
		puntiPane.setHandler(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				showVideoPane();

			}
		});

		//handler del nuovo punto pane

		nuovoPuntoPane.setHandler(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				Button source = (Button)event.getSource();
				String action = source.getText();

				if(action.toLowerCase().equals("annulla")) {

					showVideoPane();

				} else if(action.toLowerCase().equals("ok")) {
					Punto punto = null;
					try {
						punto = nuovoPuntoPane.getPunto();
						puntiPane.add(punto);

						//TODO Loader.save(videoName, puntiPane.getPunti());
						showVideoPane();
					} catch (NumberFormatException e) {
						Alert alert = new Alert(Alert.AlertType.ERROR);
						alert.showAndWait();
					}

				}

			}
		});


//////////////////////////////////////////////////////////////////////////////


		//bottone home
		homeButton = new Button("Home");

		//handler del bottone
		homeButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				//cambio il valore della property del pannello da togliere
				currentPane.setVisible(false);

				//tolgo il pannello dal root pane
				getChildren().remove(currentPane);

				//rimetto l'home pane
				setCenter(homePane);

				currentPane = homePane;
				currentPane.setVisible(true);

				//tolgo l'home button
				getChildren().remove(homeButton);

			}
		});

	}

	private String withoutExtension(String name) {

		return name.substring(0, name.length() - 4); //tolgo ".mp4"
	}

	private void showVideoPane() {

		currentPane.setVisible(false);
		videoPane.setVisible(true); //rimuove il pannello precedente automaticamente
		setCenter(videoPane);
		currentPane = videoPane;

	}

}
