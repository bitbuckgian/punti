package application;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.binding.DoubleExpression;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableDoubleValue;
import javafx.beans.value.ObservableNumberValue;
import javafx.beans.value.ObservableObjectValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.util.Duration;

public class VideoPane extends BorderPane {

	private static final int RIGHT_WIDTH = 100;
	private MediaPlayer player;
	private Button nuovoPuntoButton;
	private Button puntiButton;
	private MediaView mediaView;
	private DoubleExpression widthProperty;
	private Label timeLabel;
	private Label timeToStopLabel;
	private GridPane centerPane;
	private NumberFormat formatter;

	public VideoPane() {
		super();

		formatter = new DecimalFormat("#0");

		nuovoPuntoButton = new Button("+");
		puntiButton = new Button("punti");
		VBox buttonVBox = new VBox(puntiButton, nuovoPuntoButton);
		setRight(buttonVBox);

		Button playPauseButton = new Button(">");
		Button rewindButton = new Button("<<");
		Button forwardButton = new Button(">>");

		HBox buttonBox = new HBox(rewindButton, playPauseButton, forwardButton);

		HBox bottomBox = new HBox(buttonBox);
		setBottom(bottomBox);

		playPauseButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				System.out.println(player.getCurrentTime() + " - " + player.getStatus() + " - " + player.getStopTime());

				/*
				 * Quando è raggiunto lo stop time lo stato non cambia più; se è
				 * paused resta paused anche dopo aver invocato play() e se è playng
				 * resta playing anche se invoco pause()
				 */
				if(!(player.getCurrentTime().equals(player.getStopTime()))) {
					if(player.getStatus() == MediaPlayer.Status.PLAYING) {
						playPauseButton.setText(">");
						player.pause();
					} else if(player.getStatus() == MediaPlayer.Status.PAUSED || player.getStatus() == MediaPlayer.Status.READY) {
						playPauseButton.setText("||");
						player.play();
					} else throw new RuntimeException("Player status " + player.getStatus());
				}

			}
		});

		rewindButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				Duration time = player.getCurrentTime();
				time = time.add(Duration.seconds(-10));
				player.seek(time);

			}
		});

		forwardButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				Duration time = player.getCurrentTime();
				time = time.add(Duration.seconds(+30));
				player.seek(time);

			}
		});

		/*
		 * prima di rimuovere il video pane dal root pane viene modificato
		 * il valore della property visible. Il listener seguente viene eseguito
		 * a seguito della modifica e mette in pausa il video.
		 */
		visibleProperty().addListener(new InvalidationListener() {

			@Override
			public void invalidated(Observable arg0) {

				player.pause();
				playPauseButton.setText(">");

			}
		});


		timeLabel = new Label();
		timeToStopLabel = new Label();

		AnchorPane anchorPane = new AnchorPane();
		anchorPane.getChildren().addAll(timeLabel, timeToStopLabel);

	    AnchorPane.setRightAnchor(timeToStopLabel, 30.0);
	    //AnchorPane.setTopAnchor(timeToStopLabel, 20.0);

	    AnchorPane.setLeftAnchor(timeLabel, 30.0);
	    //AnchorPane.setTopAnchor(timeLabel, 20.0);

	    centerPane = new GridPane();
	    centerPane.add(anchorPane, 0, 1);
	    setCenter(centerPane);

	}

	//per impostare il video da mostrare
	public void setMedia(Media media) {

		player = new MediaPlayer(media);
		mediaView = new MediaView(player);
		centerPane.add(mediaView, 0, 0);

		mediaView.fitWidthProperty().bind(widthProperty.subtract(RIGHT_WIDTH));

//		double time = player.getStartTime().toSeconds();
//		timeLabel.setText(formatter.format(time));
//
//		System.out.println(player.getStopTime()); //UNKNOWN
//		double stopTime = player.getStopTime().toSeconds();
//		timeToStopLabel.setText(formatter.format(stopTime - time));

		timeLabel.setText("-");
		timeToStopLabel.setText("-");
		
		player.currentTimeProperty().addListener(new InvalidationListener() {

			@Override
			public void invalidated(Observable observable) {

				double time = ((ReadOnlyObjectProperty<Duration>) observable).get().toSeconds();

				timeLabel.setText(formatter.format(time));


				double stopTime = player.getStopTime().toSeconds();

				timeToStopLabel.setText(formatter.format(stopTime - time));
			}
		});


		//oppure:
		//timeLabel.textProperty().bind(player.currentTimeProperty().asString());
		//timeToStopLabel ... ?

	}

	public void setNuovoPuntoHandler(EventHandler<ActionEvent> handler) {
		nuovoPuntoButton.setOnAction(handler);
	}

	public void setPuntiHandler(EventHandler<ActionEvent> handler) {
		puntiButton.setOnAction(handler);
	}

	public void setPropertyToBind(ReadOnlyDoubleProperty widthProperty) {

		this.widthProperty = widthProperty;

	}

	public double getCurrentTime() {

		return player.getCurrentTime().toMillis();
	}

}
