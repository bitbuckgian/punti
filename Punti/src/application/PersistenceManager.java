package application;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import data.Content;

public class PersistenceManager {

	public static ArrayList<Punto> loadPunti(String videoName) throws ParserConfigurationException, MalformedURLException, SAXException, IOException {

		File file = new File(RootPane.userDataPath + "/" + videoName + ".xml");

		if(!file.exists()) {

			try {
				createEmptyFile(videoName);
			} catch (TransformerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		URI uri = file.toURI();

		System.out.println(uri);

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse((InputStream) uri.toURL().getContent());

		Element root;

		root = document.getDocumentElement();

		ArrayList<Punto> punti = new ArrayList<>();

		NodeList puntoNodes = root.getElementsByTagName("punto");


		for (int i = 0; i < puntoNodes.getLength(); i++) {


			Element puntoElement = (Element) puntoNodes.item(i);

			NodeList titoloList = puntoElement.getElementsByTagName("titolo");
			String titolo = titoloList.item(0).getTextContent();

			double time = Double.parseDouble(puntoElement.getAttribute("start"));

			NodeList testoList = puntoElement.getElementsByTagName("testo");
			String testo = testoList.item(0).getTextContent();

			punti.add(new Punto(titolo, time, testo));

		}

		return punti;
	}

	private static void createEmptyFile(String name) throws ParserConfigurationException, TransformerException {

		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

		// root elements
		Document doc = docBuilder.newDocument();
		Element rootElement = doc.createElement("contenuti");
		doc.appendChild(rootElement);

		// write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(RootPane.userDataPath + "/" + name + ".xml"));

		// Output to console for testing
		// StreamResult result = new StreamResult(System.out);

		transformer.transform(source, result);
	}

	public static void save(ArrayList<Punto> punti, String name) throws ParserConfigurationException, TransformerException {
		//TODO save
		if(punti == null) return;

		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

		// root elements
		Document doc = docBuilder.newDocument();
		Element rootElement = doc.createElement("contenuti");
		doc.appendChild(rootElement);

		// punto elements
		for (Punto punto : punti) {

			Element puntoElement = doc.createElement("punto");
			rootElement.appendChild(puntoElement);

			// set attribute to staff element
			Attr attr = doc.createAttribute("start");
			attr.setValue(String.valueOf(punto.getTime()));
			puntoElement.setAttributeNode(attr);

			// shorten way
			// staff.setAttribute("id", "1");

			// titolo element
			Element titoloElement = doc.createElement("titolo");
			titoloElement.appendChild(doc.createTextNode(punto.getTitolo()));
			puntoElement.appendChild(titoloElement);

			// testo element
			Element testoElement = doc.createElement("testo");
			testoElement.appendChild(doc.createTextNode(punto.getTesto()));
			puntoElement.appendChild(testoElement);

		}

		// write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(RootPane.userDataPath + "/" + name + ".xml"));

		// Output to console for testing
		// StreamResult result = new StreamResult(System.out);

		transformer.transform(source, result);

	}

	public static Content loadContent(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void save(Content content) {
		// TODO Auto-generated method stub
		
	}
}
