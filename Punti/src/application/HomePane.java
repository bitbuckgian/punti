package application;

import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;

public class HomePane extends FlowPane {

	private ArrayList<String> videos;
	private ArrayList<Button> buttons;

	public HomePane(ArrayList<String> videos) {
		this.videos = videos;

		buttons = new ArrayList<>();
		for (String string : videos) {

			Button button = new Button(string);
			getChildren().add(button);
			buttons.add(button);

		}
	}

	public void setHandler(EventHandler<ActionEvent> handler) {

		for (Button button : buttons) {
			button.setOnAction(handler);
		}

	}

}
