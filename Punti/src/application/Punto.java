package application;

public class Punto {

	private String titolo;
	private double time;
	private String testo;

	public Punto(String titolo, double time, String testo) {
		this.titolo = titolo;
		this.time = time;
		this.testo = testo;
	}

	public String getTitolo() {
		return titolo;
	}

	public double getTime() {
		return time;
	}

	public String getTesto() {
		return testo;
	}
}
