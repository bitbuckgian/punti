package application;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class NuovoPuntoPane extends VBox {

	private Button okButton;
	private Button annullaButton;
	private TextField titoloField;
	private TextField timeField;
	private TextArea testoArea;

	public NuovoPuntoPane() {
		super();

		//GUI

		HBox titoloBox = new HBox();
		Text text = new Text("Titolo");
		titoloField = new TextField();
		titoloBox.getChildren().addAll(text, titoloField);

		HBox timeBox = new HBox();
		text = new Text("Time");
		timeField = new TextField();
		timeBox.getChildren().addAll(text, timeField);

		HBox buttonsBox = new HBox();
		okButton = new Button("OK");
		annullaButton = new  Button("Annulla");
		buttonsBox.getChildren().addAll(okButton, annullaButton);

		testoArea = new TextArea();
		testoArea.setWrapText(true);

		//this è un vbox ...
		getChildren().addAll(titoloBox, timeBox, testoArea, buttonsBox);

	}

	public void setHandler(EventHandler<ActionEvent> handler) {

		okButton.setOnAction(handler);
		annullaButton.setOnAction(handler);

	}

	public Punto getPunto() throws NumberFormatException {

		double time = Double.parseDouble(timeField.getText());
		return new Punto(titoloField.getText(), time , testoArea.getText());
	}

	public void setTime(String time) {

		timeField.setText(time);

	}

}
