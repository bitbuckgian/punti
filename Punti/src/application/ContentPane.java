package application;


import java.util.ArrayList;

import data.Content;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/*
 * pannello dei contenuti: gestisce gli item
 */
public class ContentPane extends BorderPane {

	private Button videoButton;
	private ArrayList<Punto> punti;
	private VBox centerBox;
	private String videoName;

	public ContentPane() {
		super();

		this.videoName = videoName;

		videoButton = new Button("video");
		setRight(videoButton);

		centerBox = new VBox();
		ScrollPane scrollPane = new ScrollPane(centerBox);
		setCenter(scrollPane);
		scrollPane.setFitToWidth(true);
	}

	private VBox createPuntoBox(Punto punto) {

		VBox puntoBox = new VBox();

		Text titolo = new Text(punto.getTitolo());
		Text time = new Text(String.valueOf(punto.getTime()));
		Label testo = new Label(punto.getTesto());
		testo.setWrapText(true);

		puntoBox.getChildren().addAll(titolo, time, testo);

		return puntoBox;
	}

	public void setHandler(EventHandler<ActionEvent> handler) {

		videoButton.setOnAction(handler);

	}

	public void add(Punto punto) {

		punti.add(punto);
		centerBox.getChildren().add(createPuntoBox(punto));

	}

	public void setPunti(ArrayList<Punto> punti, String videoName) {

		this.punti = new ArrayList<>(punti);
		this.videoName = videoName;

		centerBox.getChildren().clear();

		for (Punto punto : punti) {

			VBox box = createPuntoBox(punto);
			centerBox.getChildren().add(box);
		}
	}

	public ArrayList<Punto> getPunti() {
		return punti == null? null :new ArrayList<Punto>(punti);
	}

	public String getVideoName() {
		return videoName;
	}

	public void setContent(Content content) {
		// TODO Auto-generated method stub

	}





}
